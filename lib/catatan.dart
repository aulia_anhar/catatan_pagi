class Catatan {
  String judul;
  String isi;

  Catatan(
    this.judul,
    this.isi,
  );

  static List<Catatan> dataDummy = [
    Catatan(
      "Agenda harian rutin",
      "Solat subuh tepat waktu dan baca quran + Ratib Al-hadad",
    ),
  ];
}
